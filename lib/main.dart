import 'package:flutter/material.dart';
import 'package:live_test_module_10/pages/newsfedd_screeb.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'News Feed',
      theme: ThemeData(
        primarySwatch: Colors.amber
      ),
      home: const NewsFeed(),

    );
  }
}

