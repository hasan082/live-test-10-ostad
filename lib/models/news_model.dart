class NewsArticle {
  final String title;
  final String source;
  final String thumbnailUrl;

  NewsArticle({
    required this.title,
    required this.source,
    required this.thumbnailUrl,
  });
}

// Sample news articles data
List<NewsArticle> articles = [
  NewsArticle(
    title: 'Flutter News 1',
    source: 'News Source 1',
    thumbnailUrl: 'https://picsum.photos/511',
  ),
  NewsArticle(
    title: 'Flutter News 2',
    source: 'News Source 2',
    thumbnailUrl: 'https://picsum.photos/512',
  ),
  NewsArticle(
    title: 'Flutter News 3',
    source: 'News Source 3',
    thumbnailUrl: 'https://picsum.photos/513',
  ),
  NewsArticle(
    title: 'Flutter News 4',
    source: 'News Source 4',
    thumbnailUrl: 'https://picsum.photos/514',
  ),
  NewsArticle(
    title: 'Flutter News 5',
    source: 'News Source 5',
    thumbnailUrl: 'https://picsum.photos/515',
  ),
  NewsArticle(
    title: 'Flutter News 6',
    source: 'News Source 6',
    thumbnailUrl: 'https://picsum.photos/516',
  ),
  NewsArticle(
    title: 'Flutter News 7',
    source: 'News Source 7',
    thumbnailUrl: 'https://picsum.photos/517',
  ),
  NewsArticle(
    title: 'Flutter News 8',
    source: 'News Source 8',
    thumbnailUrl: 'https://picsum.photos/518',
  ),
  NewsArticle(
    title: 'Flutter News 9',
    source: 'News Source 9',
    thumbnailUrl: 'https://picsum.photos/519',
  ),
  NewsArticle(
    title: 'Flutter News 10',
    source: 'News Source 10',
    thumbnailUrl: 'https://picsum.photos/520',
  ),
  // Add more news articles here...
];
