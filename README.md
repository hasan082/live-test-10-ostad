# Flutter News Feed

Flutter News Feed is a sample Flutter application that displays a news feed with adjustable layout based on the device's orientation.

## Features

- Displays a list of news articles in a feed format.
- Adjusts the layout dynamically based on device orientation (portrait or landscape).
- Shows thumbnail images, article titles, and sources for each news article.

## Screenshots

| Portrait Mode            | Landscape Mode              |
|--------------------------|-----------------------------|
| ![Portraot](potrait.png) | ![Landscape](landscape.png) |


## Getting Started

To run the application locally, follow these steps:

1. Ensure that you have Flutter SDK installed. For installation instructions, refer to the official [Flutter documentation](https://flutter.dev/docs/get-started/install).

2. Clone this repository to your local machine:
   ```
git clone https://gitlab.com/hasan082/live-test-10-ostad
   ```

3. Navigate to the project directory:
   ```
cd flutter-news-feed
   ```

4. Install the project dependencies:
   ```
flutter pub get
   ```

5. Connect a physical device or start an emulator.

6. Run the application:
   ```
flutter run
   
7. The application should now be running on your device/emulator.

## Customize

- To modify the data source or appearance of the news feed, update the `articles` list in the `lib/models/news_model.dart` file.
- Adjust the UI elements, styling, or layout in the `lib/news_feed.dart` file.

## Dependencies

The project uses the following dependencies:

- `flutter/material.dart`: The core Flutter framework.
- `flutter/widgets.dart`: Widgets for building the UI.
- Other dependencies are listed in the `pubspec.yaml` file.

## Contributing

Contributions are welcome! If you find any issues or have suggestions for improvements, please open an issue or submit a pull request.

## License

This project is licensed under the [MIT License](https://opensource.org/license/mit/).


